window.onload = function  () {
	var images = document.getElementsByClassName('super-image');
	console.log(images)	;

	for(var i = 0; i < images.length; i++) {

		if (images[i].getAttribute("data-lightbox") == "true") {
			images[i].addEventListener('click', showLightBox);
		}
	}	
}

function showLightBox () {
	console.log(this);

	var blackPanel = document.createElement("div");
		blackPanel.style.top = blackPanel.style.right = blackPanel.style.top = blackPanel.style.bottom =blackPanel.style.left = 0;
		blackPanel.id = "blackPanel";
		blackPanel.addEventListener("click", removeLightBox)

	var wrapPanel = document.createElement("div");
		wrapPanel.className = "wrapPanel clearfix";
		wrapPanel.style.width = "600px";
		wrapPanel.style.marginLeft = -parseInt(wrapPanel.style.width)/2 +'px';
		blackPanel.appendChild(wrapPanel);

	var mysuperimage = document.createElement("img");
		mysuperimage.className = "mysuperimage"
		mysuperimage.src = this.src;
		wrapPanel.appendChild(mysuperimage);

	var description = document.createElement('div');
		description.className = "description";
		description.innerText = "Some interesting information about this photo..."
		wrapPanel.appendChild(description);


	var knopka = document.createElement('div');
		knopka.className = "knopka";
		knopka.innerText = "Close"

		knopka.addEventListener('click', removeLightBox);
		wrapPanel.appendChild(knopka);
		document.body.appendChild(blackPanel);
}

function removeLightBox () {
	var blackPanel = document.getElementById("blackPanel");
		blackPanel.parentNode.removeChild(blackPanel);
}